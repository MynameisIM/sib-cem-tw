import Btn from './blocks/btn';
import Select from './blocks/select/select';
import SelectTab from './blocks/select/select-tab';
import ProductionTab from './blocks/tab/production-tab';

require('./autoload.scss');

new Btn();
new Select();
new SelectTab();
new ProductionTab();
