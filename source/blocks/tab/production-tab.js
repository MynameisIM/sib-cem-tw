class ProductionTab {
  constructor() {
    const tabLi = document.querySelectorAll('.tab__item');
    const contentBox = document.querySelectorAll('.tab__content');
    const menuline = document.querySelector('.tab__slide-elem');

    menuline.style.width = tabLi[0].offsetWidth + 'px';

    function changeContent(a) {
      for (let i = 0; i < contentBox.length; i += 1) {
        contentBox[i].classList.remove('active-tab');
      }

      switch (a) {
        case '1':
          contentBox[0].classList.add('active-tab');
          break;
        case '2':
          contentBox[1].classList.add('active-tab');
          break;
        case '3':
          contentBox[2].classList.add('active-tab');
          break;
        case '4':
          contentBox[3].classList.add('active-tab');
          break;
        default:
          break;
      }
    }

    function positionLine() {
      const currentLength = this.offsetWidth;
      menuline.style.width = currentLength + 'px';
      menuline.style.left = this.offsetLeft + 'px';
    }

    for (let i = 0; i < tabLi.length; i += 1) {
      tabLi[i].onclick = () => {
        const attr = tabLi[i].getAttribute('data-index');
        changeContent(attr);
      };
      tabLi[i].addEventListener('click', positionLine);
    }
  }
}

export default ProductionTab;
