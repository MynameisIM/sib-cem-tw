class SelectTab {
  constructor() {
    const container = document.querySelector('.select-container.select--tab');
    const options = document.querySelectorAll('.select--tab .select__option');
    const dropdown = document.createElement('ul');

    const btn = document.createElement('button');
    btn.classList.add('select__btn');
    btn.textContent = options[0].text;
    dropdown.classList.add('select__list');

    for (let i = 0; i < options.length; i += 1) {
      const dropitem = document.createElement('li');
      dropitem.classList.add('select__item');
      dropitem.textContent = options[i].value;
      dropdown.append(dropitem);
    }

    dropdown.childNodes[0].style.display = 'none';
    container.append(btn);
    container.append(dropdown);

    btn.onclick = () => {
      dropdown.classList.add('opened');
    };

    const test = document.querySelectorAll('.select--tab .select__item');

    for (let i = 0; i < test.length; i += 1) {
      test[i].onclick = () => {
        btn.textContent = test[i].textContent;
        dropdown.classList.remove('opened');
      };
    }

    document.onclick = (e) => {
      const target = e.target;
      if (target.className !== 'select__btn') {
        dropdown.classList.remove('opened');
      }
    };
  }
}

export default SelectTab;
